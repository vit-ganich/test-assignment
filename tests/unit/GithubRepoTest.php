<?php

namespace tests;

use app\models;
use app\models\GithubRepo;

/**
 * GithubRepoTest contains test classes for github repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GithubRepoTest extends \Codeception\Test\Unit
{
    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCount()
    {
        $repository = new GithubRepo("test-repo", 10, 10, 5);

        $expected = 10.0;
        $actual = $repository->getRating();

        $this->assertEquals($expected, $actual,
            sprintf("Actual rating count: '%d', expected: '%d'", $actual, $expected));
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testData()
    {
        $repository = new GithubRepo("test-repo", 10, 10, 5);

        $expectedData = [
            'name' => 'test-repo',
            'fork-count' => 10,
            'start-count' => 10,
            'watcher-count' => 5,
            'rating' => 10.0,
        ];
        $actualData = $repository->getData();

        $this->assertEquals($expectedData, $actualData, "Repo model data serialization: verification failed");
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringify()
    {
        $repository = new GithubRepo("test-repo", 10, 10, 5);

        $expected = sprintf("%-75s %4d ⇅ %4d ★ %4d 👁️", "test-repo",10, 10, 5);
        $actual = $repository->__toString();

        $this->assertEquals($expected, $actual, "Repo model __toString: verification failed");
    }
}