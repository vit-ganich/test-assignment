<?php


namespace help;


class Helpers
{
    /**
     * Auxiliary method for getting private variables
     *
     * @param $classInstance
     * @param $varName
     * @return mixed|null
     */
    public static function getPrivateVariable($classInstance, $varName)
    {
        try{
            $myClassReflection = new \ReflectionClass(get_class($classInstance));
            $secret = $myClassReflection->getProperty($varName);
            $secret->setAccessible(true);
            return $secret->getValue($classInstance);
        }
        catch (\ReflectionException $ex){
            return null;
        }
    }
}