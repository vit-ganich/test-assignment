<?php

namespace tests;

use app\components;
use app\models;

/**
 * SearcherTest contains test casess for searcher component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class SearcherTest extends \Codeception\Test\Unit
{
    /**
     * Test case for searching via several platforms
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * WARNING: I encountered an error while debugging in PHPStorm:
     *
     * Fatal error: Class PhpStorm_Codeception_ReportPrinter cannot extend from interface PHPUnit\TextUI\ResultPrinter
     * https://stackoverflow.com/questions/49059726/phpstorm-v2017-3-4-codeception-v2-4-0-incompatibility
     *
     * It seems that it's a PHPStorm bug due to incompatibility with the Codeception. I tried to fix it, but didn't
     * succeed.
     * Without debugging it was extremely difficult to test the Searcher functions.
     *
     * @return void
     */
    public function testSearcherGithub()
    {
        /* Initialize test parameters.
        There should be mocking implemented, but I'm not familiar with PHP mocking so far
        */
        $user = new models\User("kfr", "kfr", "github");
        $repos = [
            0 => new models\GithubRepo("kf-cli", 0, 2, 2),
            1 => new models\GithubRepo("cards", 0, 0, 0),
            2 => new models\GithubRepo("UdaciCards", 0, 0, 0),
            3 => new models\GithubRepo("unikgen", 0, 1, 1),
        ];
        $user->addRepos($repos);

        // Initialize search options
        $platforms = [ new components\platforms\Github([]), ];
        $users = ["kfr",];
        $searcher = new components\Searcher();

        $actual = $searcher->search($platforms, $users);
        $expected = [ $user, ];
        $this->assertEquals($expected, $actual, "Invalid search via github");
    }

    /**
     * Test case for searching via github
     *
     * @return void
     */
    public function testSearcherGitlab()
    {
        // Initialize test parameters
        $user = new models\User("5566334", "vit-ganich", "gitlab");
        $repos = [
            0 => new models\GitlabRepo("test-assignment", 0, 0),
        ];
        $user->addRepos($repos);

        // Initialize search options
        $platforms = [ new components\platforms\Gitlab([]), ];
        $users = ["vit-ganich",];
        $searcher = new components\Searcher();
        $expected = [ $user, ];
        $actual = $searcher->search($platforms, $users);
        $this->assertEquals($expected, $actual, "Invalid search via github");
    }

    /**
     * Test case for searching via Bitbucket
     */
    public function testSearcherBitbucket()
    {
        /*
         * TODO: implementation halted due to the absence of users.
         * I wasn't able to find any users for the Bitbucket, even myself.
         * User 'vhanich' exists on Bitbucket, but the search returns an empty string.
         */
    }
}