<?php

namespace tests;

use app\models\BitbucketRepo;

/**
 * BitbucketRepoTest contains test casess for bitbucket repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BitbucketRepoTest extends \Codeception\Test\Unit
{
    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCount()
    {
        $repository = new BitbucketRepo("test-repo", 10, 10);

        $expected = 15;
        $actual = $repository->getRating();
        
        $this->assertEquals($expected, $actual,
            sprintf("Actual rating count: '%d', expected: '%d'", $actual, $expected));
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testData()
    {
        $repository = new BitbucketRepo("test-repo", 10, 10);

        $expectedData = [
            'name' => 'test-repo',
            'fork-count' => 10,
            'watcher-count' => 10,
            'rating' => 15.0,
        ];
        $actualData = $repository->getData();
        
        $this->assertEquals($expectedData, $actualData, "Repo model data serialization: verification failed");
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringify()
    {
        $repository = new BitbucketRepo("test-repo", 10, 10);
        
        $expected = sprintf("%-75s %4d ⇅ %6s %4d 👁️", "test-repo", 10, "", 10);
        $actual = $repository->__toString();

        $this->assertEquals($expected, $actual, "Repo model __toString: verification failed");
    }
}