<?php

namespace tests;

use app\components\Factory;
use app\components\platforms\Github;
use app\components\platforms\Gitlab;
use app\components\platforms\Bitbucket;
use help\Helpers;
require "Helpers.php"; // to avoid [Error] Class 'help\Helpers' not found

/**
 * FactoryTest contains test classes for factory component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class FactoryTest extends \Codeception\Test\Unit
{
    /**
     * Test case for creating Github platform component
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testCreateGithub()
    {
        $factory = new Factory();
        $actual = $factory->create("github");
        $expected = new Github([]);
        $this->assertEquals($actual, $expected, "Factory: created invalid Github component");
    }

    /**
     * Test case for creating Gitlab platform component
     *
     * NOTE: I added parameter 'array $parameters = []' to the method usersProjects in Gitlab\Api\GitlabUsers.
     * Otherwise there was an error:
     * 'Declaration should be compatible with Users->usersProjects(id: int, [parameters: array = []])'
     * @return void
     */
    public function testCreateGitlab()
    {
        $factory = new Factory();
        $actual = $factory->create("gitlab");
        $expected = new Gitlab([]);
        $this->assertEquals($actual, $expected, "Factory: created invalid Gitlab component");
    }

    /**
     * Test case for creating Bitbucket platform component
     *
     * @return void
     */
    public function testCreateBitbucket()
    {
        $factory = new Factory();
        $actual = $factory->create("bitbucket");
        $expected = new Bitbucket([]);
        $this->assertEquals($actual, $expected, "Factory: created invalid Bitbucket component");
    }

    /**
     * @return void
     */
    public function testValidateExistingKeysAreGettingFromCache()
    {
        $factory = new Factory();
        // Create several components with the same name to ensure that duplicates are getting from cache
        for($i = 0; $i <= 2; $i++){
            $factory->create("github");
            $factory->create("gitlab");
            $factory->create("bitbucket");
        }

        /* As 'cahce' property is private, I had to perform such a complex operations to get it.
        I believe, it would be impossible in other languages such as C# or Java without setting
        access modifier to protected or public.
        */
        $cacheArr = Helpers::getPrivateVariable($factory, "cahce");

        $this->assertEquals(3, count($cacheArr));
    }

    /**
     * Test case for creating platform component with wrong parameters
     * @return void
     */
    public function testNegativeUnknownPlatform()
    {
        $this->expectException(\LogicException::class);
        $factory = new Factory();
        $factory->create("tfs");
    }

    /**
     * Test case for component name with uppercase and whitespaces
     * in case of implemented trim() and strtolower() methods
     * @return void
     */
    public function testNegativePlatformNameWithUppercaseAndWhitespace()
    {
        $this->expectException(\LogicException::class);
        $factory = new Factory();
        $factory->create(" gItHub ");
    }
}