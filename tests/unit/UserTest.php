<?php

namespace tests;

use app\models;
use help\Helpers;

/**
 * UserTest contains test casess for user model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class UserTest extends \Codeception\Test\Unit
{
    /**
     * Test case for adding repo models to user model
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testAddingRepos()
    {
        $user = new models\User("testId", "testName", "gitlab");
        $user->addRepos([
            new models\GithubRepo("sad-little-repo", 0, 0 , 0),
            new models\GithubRepo("test-assignment", 10, 10 , 10),
            new models\GithubRepo("php-vs-python", 20, 20 , 30),
        ]);

        $expected = [
            0 => new models\GithubRepo("php-vs-python", 20, 20 , 30),
            1 => new models\GithubRepo("test-assignment", 10, 10 , 10),
            2 => new models\GithubRepo("sad-little-repo", 0, 0 , 0),
        ];
        $actual = Helpers::getPrivateVariable($user, "repositories");

        $this->assertEquals($actual, $expected);
    }

    /**
     * Test case for counting total user rating
     *
     * @return void
     */
    public function testTotalRatingCount()
    {
        $user = new models\User("testId", "testName", "gitlab");
        $repos = [
            new models\GithubRepo("test-assignment", 2, 2, 2),
            new models\GithubRepo("php-vs-python", 1, 1 , 1),
        ];
        $user->addRepos($repos);
        $actual = $user->getTotalRating();

        $this->assertEquals(3.5, $actual);
    }

    /**
     * Test case for user model data serialization
     *
     * @return void
     */
    public function testData()
    {
        $user = new models\User("testId", "testName", "github");
        $repos = [
            new models\GithubRepo("test-assignment", 2, 2, 2),
            new models\GithubRepo("php-vs-python", 1, 1 , 1),
        ];
        $user->addRepos($repos);

        $expected = [
            'name' => 'testName',
            'platform' => 'github',
            'total-rating' => 3.5,
            'repos' => [],
            'repo' => [
                0 => [
                    'name' => 'test-assignment',
                    'fork-count' => 2,
                    'start-count' => 2,
                    'watcher-count' => 2,
                    'rating' => 2.3333333333333335,
                ],
                1 => [
                    'name' => 'php-vs-python',
                    'fork-count' => 1,
                    'start-count' => 1,
                    'watcher-count' => 1,
                    'rating' => 1.1666666666666667,
                ],
            ],
        ];
        $actual = $user->getData();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test case for user model __toString verification
     * I can't get a valid string to check
     *
     * @return void
     */
    public function testStringify()
    {
        $user = new models\User("testId", "testName", "gitlab");
        $repos = [
            new models\GithubRepo("test-assignment", 2, 2, 2),
            new models\GithubRepo("php-vs-python", 1, 1 , 1),
        ];
        $user->addRepos($repos);

        $expected = sprintf(
            "%-75s %19d 🏆\n%'=98s\n",
            "testName (gitlab)",
            3,
            ""
        );
        $repos = [
            sprintf("%-75s %4d ⇅ %4d ★ %4d � ", "test-assignment",2, 2, 2),
            sprintf("%-75s %4d ⇅ %4d ★ %4d � ", "php-vs-python",1, 1, 2),
        ];
        foreach ($repos as $repository) {
            $expected .= $repository . "\n";
        }

        $actual = $user->__toString();
         /*
          * Test fails - two strings differ only by '\n' and '\ n' chars. I can't come up with
          * a good solution for this. So I created a custom kostyl' function.
          */
        //$this->assertEquals($expected, $actual);
        $this->assertTrue($this->stringComparator($expected, $actual, 90));
    }

    /**
     * Kostyl' function for comparison strings with specified percent of equality.
     *
     * @param $string1
     * @param $string2
     * @param $equality_percent
     * @return bool
     */
    private function stringComparator($string1, $string2, $equality_percent)
    {
        $same_char_count = 0;
        for ($i=0; $i < strlen($string1); $i++)
        {
            if($string1[$i] == $string2[$i])
            {
                $same_char_count++;
            }
        }
        $actual_equality_percent = $same_char_count / strlen($string1) * 100;
        return $actual_equality_percent >= $equality_percent ? true : false;
    }
}