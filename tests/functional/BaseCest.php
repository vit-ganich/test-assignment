<?php

use Codeception\Util\HttpCode;

/**
 * Base contains test cases for tesing api endpoint 
 * 
 * @see https://codeception.com/docs/modules/Yii2
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BaseCest
{
    /**
     * Example test case
     *
     * @return void
     */
    public function testExample(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'kfr',
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $expected = json_decode('[
            {
                "name": "kfr",
                "platform": "github",
                "total-rating": 1.5,
                "repos": [],
                "repo": [
                    {
                        "name": "kf-cli",
                        "fork-count": 0,
                        "start-count": 2,
                        "watcher-count": 2,
                        "rating": 1
                    },
                    {
                        "name": "cards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "UdaciCards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "unikgen",
                        "fork-count": 0,
                        "start-count": 1,
                        "watcher-count": 1,
                        "rating": 0.5
                    }
                ]
            }
        ]');
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with bad request params
     *
     * @return void
     */
    public function testBadParams(\FunctionalTester $I)
    {
        $I->assertTrue(
            $this->checkExceptionThrown(LogicException::class, function() use ($I) {
                $I->amOnPage([
                    'base/api',
                    'users' => [
                        'xxx',
                    ],
                    'platforms' => [
                        'xxx',
                    ]
                ]);
            })
        );
    }

    /**
     * Test case for api with empty user list
     *
     * @return void
     */
    public function testEmptyUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [],
            'platforms' => [
                'gitlab',
            ]
        ]);
        $expected = "<pre>Bad Request: Missing required parameters: users</pre>";
        $I->assertEquals($expected, html_entity_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with empty platform list
     *
     * @return void
     */
    public function testEmptyPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'frog',
            ],
            'platforms' => [],
        ]);
        $expected = "<pre>Bad Request: Missing required parameters: platforms</pre>";
        $I->assertEquals($expected, html_entity_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with non empty platform list
     *
     * @return void
     */
    public function testSeveralPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'hamster',
            ],
            'platforms' => [
                'github',
                'gitlab',
            ],
        ]);

        $expected = json_decode('[
    {
        "name": "hamster",
        "platform": "github",
        "total-rating": 13.5,
        "repos": [],
        "repo": [
            {
                "name": "DC801-SAINTCON2017-Minibadge",
                "fork-count": 5,
                "start-count": 1,
                "watcher-count": 1,
                "rating": 3.8333333333333335
            },
            {
                "name": "DefCon23",
                "fork-count": 2,
                "start-count": 4,
                "watcher-count": 4,
                "rating": 3.3333333333333335
            },
            {
                "name": "TubeCube",
                "fork-count": 0,
                "start-count": 2,
                "watcher-count": 2,
                "rating": 1
            },
            {
                "name": "classroom",
                "fork-count": 0,
                "start-count": 3,
                "watcher-count": 3,
                "rating": 1.5
            },
            {
                "name": "dc22-dc801-party-badge",
                "fork-count": 1,
                "start-count": 2,
                "watcher-count": 2,
                "rating": 1.6666666666666667
            },
            {
                "name": "DC25PartyBadge",
                "fork-count": 1,
                "start-count": 1,
                "watcher-count": 1,
                "rating": 1.1666666666666667
            },
            {
                "name": "Defcon27-Badge",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "trevorforget",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "SpaceKitty",
                "fork-count": 0,
                "start-count": 1,
                "watcher-count": 1,
                "rating": 0.5
            },
            {
                "name": "SeggerAdapter",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "qmk_firmware",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "minibadge",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "Adafruit_nRF52_Bootloader",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "Defcon26-Badge",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "addons",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "DC26PartyBadge",
                "fork-count": 0,
                "start-count": 1,
                "watcher-count": 1,
                "rating": 0.5
            },
            {
                "name": "DC24PartyBadge",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "BuzzyBee",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "bee-merry",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "badges",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "twinkletwinkie",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            }
        ]
    },
    {
        "name": "hamster",
        "platform": "gitlab",
        "total-rating": 0,
        "repos": [],
        "repo": [
            {
                "name": "Multiview",
                "fork-count": 0,
                "start-count": 0,
                "rating": 0
            }
        ]
    }
]');

        $I->assertEquals($expected,json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with non empty user list
     *
     * @return void
     */
    public function testSeveralUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'hamster',
                'vit-ganich',
            ],
            'platforms' => [
                'gitlab',
            ],
        ]);

        $expected = json_decode('[
    {
        "name": "hamster",
        "platform": "gitlab",
        "total-rating": 0,
        "repos": [],
        "repo": [
            {
                "name": "Multiview",
                "fork-count": 0,
                "start-count": 0,
                "rating": 0
            }
        ]
    },
    {
        "name": "vit-ganich",
        "platform": "gitlab",
        "total-rating": 0,
        "repos": [],
        "repo": [
            {
                "name": "test-assignment",
                "fork-count": 0,
                "start-count": 0,
                "rating": 0
            }
        ]
    }
]');

        $I->assertEquals($expected,json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with unknown platform in list
     *
     * @return void
     */
    public function testUnknownPlatforms(\FunctionalTester $I)
    {
        $I->assertTrue(
            $this->checkExceptionThrown(LogicException::class, function() use ($I) {
                $I->amOnPage([
                    'base/api',
                    'users' => [
                        'hamster',
                    ],
                    'platforms' => [
                        'gitlab',
                        'github',
                        'team-foundation',
                    ],
                ]);
            })
        );
    }

    /**
     * Test case for api with unknown user in list
     *
     * @return void
     */
    public function testUnknownUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'unknownuser1111111',
                'qwerty12345567890',
            ],
            'platforms' => [
                'gitlab',
                'github',
                'bitbucket',
            ],
        ]);

        $I->assertEquals([],json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with mixed (unknown, real) users and non empty platform list
     *
     * @return void
     */
    public function testMixedUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'unknownuser1111111',
                'qwerty12345567890',
                'hamster',
            ],
            'platforms' => [
                'gitlab',
                'bitbucket',
            ],
        ]);
        $expected = json_decode('[
    {
        "name": "hamster",
        "platform": "gitlab",
        "total-rating": 0,
        "repos": [],
        "repo": [
            {
                "name": "Multiview",
                "fork-count": 0,
                "start-count": 0,
                "rating": 0
            }
        ]
    }
]');
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with mixed (github, gitlab, bitbucket) platforms and non empty user list
     *
     * @return void
     */
    public function testMixedPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'anton',
            ],
            'platforms' => [
                'github',
                'gitlab',
                'bitbucket',
            ],
        ]);
        $expected = json_decode('[
    {
        "name": "anton",
        "platform": "github",
        "total-rating": 1,
        "repos": [],
        "repo": [
            {
                "name": "9pm",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "bit-squatting",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "bsddrivers",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "data-structures",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "FairRank",
                "fork-count": 0,
                "start-count": 1,
                "watcher-count": 1,
                "rating": 0.5
            },
            {
                "name": "gamepad",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "gtypist_se",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "kactl",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "rss_aggregator",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "sort",
                "fork-count": 0,
                "start-count": 0,
                "watcher-count": 0,
                "rating": 0
            },
            {
                "name": "static_analysis",
                "fork-count": 0,
                "start-count": 1,
                "watcher-count": 1,
                "rating": 0.5
            }
        ]
    },
    {
        "name": "anton",
        "platform": "gitlab",
        "total-rating": 0,
        "repos": [],
        "repo": [
            {
                "name": "minecraft-backup",
                "fork-count": 0,
                "start-count": 0,
                "rating": 0
            }
        ]
    }
]');
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Kostyl' function for exception handling.
     * I tried $I->expect(prediction: LogicException) and $I->expectException,
     * but it didn't work for me.
     *
     * @param $exception
     * @param $function
     * @return bool
     */
    private function checkExceptionThrown($exception, $function)
    {
        try
        {
            $function();
            return false;
        }
        catch (Exception $ex)
        {
            if (get_class($ex) == $exception)
            {
                return true;
            }
            return false;
        }
    }
}